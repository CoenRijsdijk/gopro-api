const mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;
const { logger, dbHost, dbPort, dbUser, dbDatabase, dbPassword } = require('./config');

// Gebruik es6 promises ipv mongoose mpromise
mongoose.Promise = global.Promise;

//if (process.env.NODE_ENV == 'testCloud' || process.env.NODE_ENV == 'production') {
console.log("mongo online")
mongoose.connect('mongodb+srv://admin:1234@cluster-p1nla.mongodb.net/gopro?retryWrites=true&w=majority', { useNewUrlParser: true })

//else if (process.env.NODE_ENV !== 'test') {*/
//console.log("mongo local")
//mongoose.connect('mongodb://localhost/gopro')
//}

/*const dbUrl = process.env.NODE_ENV === 'production' ?
    'mongodb://' + dbUser + ':' + dbPassword + '@' + dbHost + ':' + dbPort + '/' + dbDatabase :
    'mongodb://localhost/' + dbDatabase;

mongoose.connect(dbUrl, {
    useCreateIndex: true,
    useNewUrlParser: true
})*/
var connection = mongoose.connection
    .once('open', () => logger.info(`Connected to Mongo on ${dbHost}:${dbPort}/${dbDatabase}`))
    .on('error', (error) => logger.error(error.toString()));
module.exports = connection;