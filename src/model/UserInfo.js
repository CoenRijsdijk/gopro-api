//
// Class structuring information about a user.
//
'use strict';

class UserInfo {

    constructor(_id, name, email, token, birthday, street, housenumber, postalcode, city, country) {
        this._id = _id
        this.name = name;
        this.email = email;
        this.token = token;
        this.birthday = birthday
        this.street = street
        this.housenumber = housenumber
        this.postalcode = postalcode
        this.city = city
        this.country = country
    }

}

module.exports = UserInfo;