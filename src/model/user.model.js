const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const OrderSchema = require('./order.model').default

const UserSchema = new Schema({

    email: {
        type: String,
        unique: true,
        validate: {
            validator: (email) => email.length > 2,
            message: 'Email must be valid.'
        },
        required: [true, 'Email is required.']
    },

    name: {
        firstname: {
            type: String,
            validate: {
                validator: (firstname) => firstname.length > 2,
                message: 'firstname must be valid.'
            },
            required: [true, 'firstname is required.']
        },
        lastname: {
            type: String,
            validate: {
                validator: (lastname) => lastname.length > 2,
                message: 'lastname must be valid.'
            },
            required: [true, 'lastname is required.']
        },
    },

    password: {
        type: String,
        validate: {
            validator: (password) => password.length > 5,
            message: 'password must be valid.'
        },
        required: [true, 'password is required.']
    },
    birthday: {
        type: Date,
        required: [true, 'birthday is required']
    },
    street: {
        type: String,
        required: [true, 'street is required']
    },
    housenumber: {
        type: String,
        required: [true, 'housenumber is required']
    },
    postalcode: {
        type: String,
        required: [true, 'postalcode is required']
    },
    city: {
        type: String,
        required: [true, 'city is required']
    },
    country: {
        type: String,
        required: [true, 'country is required']
    },
    orders: {
        type: [OrderSchema],
        default: []
    }
}, {
    timestamps: true
});

// when a user is deleted all their reviews need to be deleted
// note: use an anonymous function and not a fat arrow function here!
// otherwise 'this' does not refer to the correct object
// use 'next' to indicate that mongoose can go to the next middleware
UserSchema.pre('remove', function (next) {
    // include the product model here to avoid cyclic inclusion
    const Product = mongoose.model('product');

    // don't iterate here! we want to use mongo operators!
    // this makes sure the code executes inside mongo
    Product.updateMany({}, { $pull: { 'reviews': { 'user': this._id } } })
        .then(() => next());
});

const User = mongoose.model('user', UserSchema);
module.exports = User;