const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ItemSchema = require('./item.model')

const OrderSchema = new Schema({
    orderStatus: {
        type: String,
        required: [true, 'Order needs a status']
    },
    orderDate: {
        type: Date,
        required: [true, 'Order needs a date']
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: [true, 'Order needs a user']
    },
    products: {
        type: [ItemSchema],
        required: [true, 'Order needs items']
    }
})

module.exports = OrderSchema
