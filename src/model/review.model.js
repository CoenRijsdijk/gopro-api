const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ProductSchema = require('../model/product.model')

const ReviewSchema = new Schema({
    title: {
        type: String,
        required: [true, 'A title is required.'],
    },
    description: {
        type: String,
        required: [true, 'A description is required.'],
    },
    rating: {
        type: Number,
        required: [true, 'A rating is required.'],
        validate: {
            validator: (rating) => {
                return Number.isInteger(rating) && 0 <= rating && rating <= 5
            },
            message: 'A rating can only be 1, 2, 3, 4 or 5 stars'
        }
    },
    postingDate: {
        type: Date,
        required: [true, 'A rating needs a posting date']
    },

    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to be attached to a review.'],
        ref: 'user'
    }
});

module.exports = ReviewSchema;