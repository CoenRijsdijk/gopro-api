const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, 'A category needs a name']
    }
})

const Category = mongoose.model('category', CategorySchema)

module.exports = Category;