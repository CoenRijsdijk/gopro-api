const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = require('./review.model')

const ProductSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A product needs a name']
    },
    description: String,
    price: {
        type: Number,
        required: [true, 'A product needs a price'],
        validate: {
            validator: (price) => price >= 0,
            message: 'A price needs to be positive'
        }
    },
    imageUrl: {
        type: String,
        required: [true, 'A products needs an image']
    },
    reviews: {
        type: [ReviewSchema],
        default: []
    },
    category: {
        type: Schema.Types.ObjectId,
        required: [true, 'A Product needs a category'],
        ref: 'category'
    }
})

ProductSchema.virtual('rating').get(function () {
    if (this.reviews.length === 0) {
        return "no rating"
    } else {
        let sum = 0
        for (let review of this.reviews) {
            sum += review.rating
        }
        return sum / this.reviews.length
    }
})

ProductSchema.set("toJSON", { getters: true })
ProductSchema.set('toObject', { getters: true })

const Product = mongoose.model('product', ProductSchema)

module.exports = Product;