const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    quantity: {
        type: String,
        required: [true, 'street is required']
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'product',
        required: [true, 'Item needs a product']
    }
})

module.exports = ItemSchema
