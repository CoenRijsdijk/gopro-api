//
// User routes
//
'use strict';

let routes = require('express').Router()
let ReviewController = require('../controllers/review.controller')
const validateToken = require("../controllers/authentication.controller")
    .validateToken;

routes.post('/product/:id/review', validateToken, ReviewController.createReview)

module.exports = routes;