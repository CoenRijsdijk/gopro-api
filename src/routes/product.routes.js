//
// User routes
//
'use strict';

let routes = require('express').Router()
let ProductController = require('../controllers/product.controller')

routes.post('/products', ProductController.createProduct)
routes.get('/products', ProductController.getAllProducts)
routes.get('/products/:id', ProductController.getProductById)
routes.get('/products/category/:catId/', ProductController.getProductsByCategory)

module.exports = routes;