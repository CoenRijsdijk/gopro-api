//
// User routes
//
'use strict';

let routes = require('express').Router()
let OrderController = require('../controllers/order.controller')
const validateToken = require("../controllers/authentication.controller")
    .validateToken;

routes.post('/order', validateToken, OrderController.createOrder)
routes.get('/order/me', validateToken, OrderController.getAllOrdersByUserId)

module.exports = routes;