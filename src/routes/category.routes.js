//
// User routes
//
'use strict';

let routes = require('express').Router()
let CategoryController = require('../controllers/category.controller')

routes.post('/category', CategoryController.createCategory)
routes.get('/category', CategoryController.getAllCategories)
routes.get('/category/:id', CategoryController.getCategoryById)

module.exports = routes;