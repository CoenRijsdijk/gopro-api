//
// CRUD operations on person
//
'use strict';

const assert = require('assert')
const auth = require('../util/auth/authentication')
const authController = require('../controllers/authentication.controller')
const User = require('../model/user.model')
const Order = require('../model/order.model')
const ApiError = require('../model/ApiError')
const logger = require('../config/config').logger

module.exports = {
    createOrder(req, res, next) {
        const orderProps = req.body
        const userId = req.user.id;

        const order = {
            orderStatus: orderProps.orderStatus,
            orderDate: Date.now(),
            products: orderProps.products,
            user: userId
        }
        logger.trace('order = ' + order.products);
        User.updateOne({ _id: userId }, {
            $push: {
                orders: order
            }
        }, { runValidators: true })
            .then(order => res.status(200).json(order).end())
            .catch(error => next(new ApiError(error)))
    },

    getAllOrdersByUserId(req, res, next) {
        const userId = req.user.id;
        User.findOne({ _id: userId }).select({ orders: 1 })
            .populate([
                {
                    path: 'orders.products.item.product',
                    model: 'product',
                }
            ])
            .then(orders => res.status(200).json({ result: orders }).end())
            .catch(error => next(new ApiError(error)))

    }
}