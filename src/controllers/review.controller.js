//
// CRUD operations on person
//
'use strict';

const assert = require('assert')
const auth = require('../util/auth/authentication')
const authController = require('../controllers/authentication.controller')
const Review = require('../model/review.model').default
const User = require('../model/user.model')
const Product = require('../model/product.model')
const ApiError = require('../model/ApiError')
const logger = require('../config/config').logger

module.exports = {
    createReview(req, res, next) {
        const productId = req.params.id
        const reviewProps = req.body;
        const userId = req.user.id;
        logger.trace('by user ' + userId + " on product " + productId + req.header("x-access-token"));

        const review = {
            title: reviewProps.title,
            description: reviewProps.content,
            rating: reviewProps.rating,
            postingDate: Date.now(),
            user: userId
        }

        logger.trace('review = ' + review.description + review.postingDate);

        Product.updateOne({ _id: productId }, {
            $push: {
                reviews: review
            }
        }, { runValidators: true })
            .then(product => res.status(200).json(product).end())
            .catch(error => next(new ApiError(error)))
    }
}