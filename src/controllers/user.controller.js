//
// CRUD operations on person
//
'use strict';

const assert = require('assert')
const auth = require('../util/auth/authentication')
const User = require('../model/user.model')
const UserInfo = require('../model/UserInfo')
const ApiError = require('../model/ApiError')
const logger = require('../config/config').logger
const bcrypt = require('bcryptjs')

const BCRYPT_SALT_ROUNDS = 12;

module.exports = {

    /**
     * Create a new person and add it to the list.
     * 
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    createUser(req, res, next) {

        const userProps = req.body;
        logger.trace('user = ' + userProps);

        User.create(userProps)
            .then(user => res.status(200).json(user).end())
            .catch(error => next(new ApiError(error)))
    },

    /**
     * Get the current list of persons.
     * 
     */
    getAllUsers(req, res, next) {
        logger.info('User info in getAllPersons: ' + req.user)

        User.find({}, 'name email')
            .then(users => res.status(200).json(users).end())
            .catch(error => next(new ApiError(error)))
    },

    /**
     * Replace an existing person in the list. We need an id and a new person 
     * object. The new person will be stored at index id.
     * 
     * @param {*} req req.params.id is the person's id in the personlist. req.body contains the new person object.
     * @param {*} res The updated person object.
     * @param {*} next ApiError when id and/or person object are invalid.
     */
    updateUserById(req, res, next) {
        const id = req.params.id
        const user = req.body
        console.log(id)
        console.log(user)
        console.log(req.header("x-access-token"))
        bcrypt.hash(user.password, BCRYPT_SALT_ROUNDS)
            .then(hashedPassword => {
                user.password = hashedPassword
                return User.findByIdAndUpdate(id, user)
            })
            .then(user => {
                logger.info('Updated user: ' + user.name)
                // Create an object containing the data we want in the payload.
                const payload = {
                    email: user.email,
                    id: user._id,
                    name: user.name,
                    birthday: user.birthday,
                    street: user.street,
                    housenumber: user.housenumber,
                    postalcode: user.postalcode,
                    city: user.city,
                    country: user.country
                }

                // Userinfo returned to the caller.
                const userinfo = new UserInfo(user._id, user.name, user.email, auth.encodeToken(payload),
                    user.birthday, user.street, user.housenumber, user.postalcode, user.city, user.country)
                res.status(200).json(userinfo).end()

            })
            .catch(error => {
                logger.info('In catch: error = ' + error)
                next(new ApiError(error, 500))
            })
    },

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    deleteUserById(req, res, next) {
        const id = req.params.id
        User.findByIdAndDelete(id)
            .then(user => res.status(200).json({ "userDeleted": user }).end())
            .catch(error => next(new ApiError(error)))
    },

    /**
     * Return the token, username, user email and user profile picture url. 
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */
    getUserProfile(req, res, next) {

        logger.trace(`getUserProfile id = ${req.user.id}`)

        User.findById(req.user.id)
            .then(user => {
                logger.info('Found user: ' + user.name + user)

                // Create an object containing the data we want in the payload.
                // We generate a new token here.
                const payload = {
                    email: user.email,
                    id: req.user.id,
                    name: user.name,
                    birthday: user.birthday,
                    street: user.street,
                    housenumber: user.housenumber,
                    postalcode: user.postalcode,
                    city: user.city,
                    country: user.country
                }
                // Userinfo returned to the caller.
                const userinfo = new UserInfo(user.name, user.email, auth.encodeToken(payload),
                    user.birthday, user.street, user.housenumber, user.postalcode, user.city, user.country)
                res.status(200).json(userinfo).end()
            })
            .catch(error => {
                logger.info('In catch: error = ' + error)
                next(new ApiError(error, 500))
            })
    },
    /**
     * Return the token, username, user email and user profile picture url. 
     * 
     * @param {object} req 
     * @param {object} res 
     * @param {function} next 
     */

    getUserById(req, res, next) {
        const id = req.params.id;
        logger.trace(`getUserProfile id =` + id)
        User.findById(id)
            .then(user => res.status(200).json(user).end())
            .catch(error => next(new ApiError(error)))
    }

}