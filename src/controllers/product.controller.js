//
// CRUD operations on person
//
'use strict';

const assert = require('assert')
const auth = require('../util/auth/authentication')
const Product = require('../model/product.model')
const User = require('../model/user.model')
const ApiError = require('../model/ApiError')
const logger = require('../config/config').logger

module.exports = {
    createProduct(req, res, next) {
        const productProps = req.body;
        logger.trace('product = ' + productProps);

        Product.create(productProps)
            .then(product => res.status(200).json(product).end())
            .catch(error => next(new ApiError(error)))
    },
    getAllProducts(req, res, next) {
        logger.info('getAllProducts')

        Product.find()
            .then(products => res.status(200).json({ result: products }).end())
            .catch(error => next(new ApiError(error)))
    },
    getProductById(req, res, next) {
        logger.info("Get product by id")
        console.log(req.params.id)
        const id = req.params.id;

        Product.findOne({ _id: id }).populate({
            path: 'reviews.user',
            model: 'user'
        }).populate({
            path: 'category',
            model: 'category'
        })
            .then(product => res.status(200).json(product).end())
            .catch(error => next(new ApiError(error)))

    },
    getProductsByCategory(req, res, next) {
        logger.info("Get prod by cat")
        console.log(req.params.catId)
        const catId = req.params.catId
        const catName = req.params.catName

        Product.find({ category: { _id: catId } })
            .then(product => res.status(200).json({ result: product }).end())
            .catch(error => next(new ApiError(error)))
        //logger.info(catController.getCategoryById)
    }
}