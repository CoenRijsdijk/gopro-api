//
// CRUD operations on person
//
'use strict';

const assert = require('assert')
const Category = require('../model/category.model')
const ApiError = require('../model/ApiError')
const logger = require('../config/config').logger

module.exports = {
    createCategory(req, res, next) {
        logger.info("Create category")
        const categoryProps = req.body;
        logger.trace('category = ' + categoryProps);

        Category.create(categoryProps)
            .then(cat => res.status(200).json(cat).end())
            .catch(error => next(new ApiError(error)))
    },

    getAllCategories(req, res, next) {
        logger.info("Get all categories called")
        Category.find()
            .then(categories => res.status(200).json({ result: categories }).end())
            .catch(error => next(new ApiError(error)))
    },

    getCategoryById(req, res, next) {
        logger.info("Category by id called")
        const id = req.params.id
        Category.findOne({ _id: id })
            .then(category => res.status(200).json(category).end())
            .catch(error => next(new ApiError(error)))
    }
}