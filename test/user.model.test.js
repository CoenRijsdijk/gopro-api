const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('../src/model/user.model')

describe('user model', function () {
    it('should reject a missing user name', async function () {
        const user = new User({})

        await expect(user.save()).to.be.rejectedWith(Error)
    })

    xit('should create an empty bought list by default', async function () {
        const user = await new User(
            {
                name: {
                    firstname: 'John',
                    lastname: 'Doe'
                },
                email: 'johndoe@jd.com',
                password: 'jd123456'
            }
        ).save()

        expect(user).to.have.property('bought').and.to.be.empty
    })
})